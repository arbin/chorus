#!/usr/bin/env python
__script_name__ = 'excel.py'
__author__ = 'Arbin Bulaybulay (arbin.bulaybulay@spi-global.com)'
__about__ = 'Process spreadsheets to get the sheet names and its items.'

from openpyxl import load_workbook


def get_sheet_names(workbook):
    """Returns list of sheet names """
    wb2 = load_workbook(workbook)
    return wb2.get_sheet_names()


def get_sheet_ranges(workbook, sheet_name, cell_number):
    """Returns cell value. """
    wb = load_workbook(filename=workbook)
    sheet_ranges = wb[sheet_name]
    return sheet_ranges[cell_number].value


def get_all_items(workbook, sheet_name, column_number):
    """Returns number of rows and list of dictionaries for all cell name
    and value in a column.
    """
    wb = load_workbook(workbook)
    ws = wb.get_sheet_by_name(sheet_name)
    data = []
    count = 0
    for row in range(2, ws.max_row + 1):
        for column in column_number:
            cell_name = "{}{}".format(column, row)
            data.append({column + str(row): ws[cell_name].value})
            count += 1
    return count, data


def get_all_data(workbook):
    """Returns number of rows and list of dictionaries for all cell name
    and value in a column
    """
    wb = load_workbook(workbook)
    sheets = get_sheet_names(file)
    all_data = []
    for sheet_name in sheets:
        ws = wb.get_sheet_by_name(sheet_name)
        first_row = ws[1]
        data = []
        count = 0
        for col in first_row:
            if 'URL' in str(col.value):
                for row in range(2, ws.max_row + 1):
                    for column in col.column:
                        cell_name = "{}{}".format(column, row)
                        data.append({column + str(row): ws[cell_name].value})
                        count += 1
        all_data.append({sheet_name: data})
    return all_data


def get_url_column(workbook, sheet_name):
    """Returns number of rows and list of dictionaries for all cell
    name and value in a column automatically get all items with a column
    header that has URL"""
    wb = load_workbook(workbook)
    ws = wb.get_sheet_by_name(sheet_name)
    first_row = ws[1]
    data = []
    count = 0
    for col in first_row:
        if 'URL' in str(col.value):
            for row in range(2, ws.max_row + 1):
                for column in col.column:
                    cell_name = "{}{}".format(column, row)
                    data.append({column + str(row): ws[cell_name].value})
                    count += 1
    wb.close()
    return count, data


def update_cell_value(workbook, sheet_name, key, cell_value):
    """Update a cell value """
    wb = load_workbook(workbook)
    ws = wb.get_sheet_by_name(sheet_name)
    ws[key].value = cell_value
    wb.save(workbook)


def insert_column(workbook):
    """Insert a column """
    wb = load_workbook(workbook)
    for ws in wb.worksheets:
        for index, row in enumerate(ws.rows, start=1):
            print(index)


if __name__ == "__main__":
    input_file = r'C:\Users\abulaybulay\PycharmProjects\LexisAdvancePermaLinkTestTool\in\LA Permamlink Test with bad links.xlsx'
    #print(get_sheet_names(file))
    #print(get_sheet_ranges(file))
    #print(get_sheet_ranges(file, 'Cases', 'B2'))
    #print(get_all_items(file, 'Cases', 'B'))
    #count, data = get_all_items(file, 'Cases', 'B')
    #update_cell_value(file, 'Cases', data)
    #get_url_column(file, 'Cases - Test1')
    #insert_column(file)
    get_all_data(input_file)