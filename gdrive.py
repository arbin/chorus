"""
__name__ = 'gdrive.py'
__author__ = 'abulaybulay'
__about__ = 'Google drive upload and share'
"""

import httplib2
import os

from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage
from apiclient.http import MediaFileUpload

try:
    import argparse
    flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
except ImportError:
    flags = None


SCOPES = 'https://www.googleapis.com/auth/drive'
CLIENT_SECRET_FILE = os.getcwd() + '/creds/client_secret.json'
APPLICATION_NAME = 'Chorus'


def get_credentials():
    """Gets valid user credentials from storage.

    If nothing has been stored, or if the stored credentials are invalid,
    the OAuth2 flow is completed to obtain the new credentials.

    Returns:
        Credentials, the obtained credential.
    """
    home_dir = os.path.expanduser('~')
    credential_dir = os.path.join(home_dir, '.credentials')
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    credential_path = os.path.join(credential_dir,
                                   'chorus.json')

    store = Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
        flow.user_agent = APPLICATION_NAME
        if flags:
            credentials = tools.run_flow(flow, store, flags)
        else:
            credentials = tools.run(flow, store)
        print('Storing credentials to ' + credential_path)
    return credentials


def share(service, file_id):
    def callback(request_id, response, exception):
        if exception:
            # Handle error
            print(exception)
        else:
            print("Permission Id: %s" % response.get('id'))

    batch = service.new_batch_http_request(callback=callback)
    user_permission = {
        'type': 'user',
        'role': 'writer',
        'emailAddress': '495101410258-compute@developer.gserviceaccount.com'
    }
    batch.add(service.permissions().create(
        fileId=file_id,
        body=user_permission,
        fields='id',
    ))
    batch.execute()


def upload(title, filename, google_mime, mime_type, reader_email):
    """Upload a file to Google drive and convert to a
    google file type. Returns the file id
    """
    credentials = get_credentials()
    http = credentials.authorize(httplib2.Http())
    service = discovery.build('drive', 'v3', http=http)
    file_metadata = {
      'name': title,
      'mimeType': google_mime
    }
    media = MediaFileUpload(filename,
                            mimetype=mime_type,
                            resumable=True)
    file_id = service.files().create(body=file_metadata, media_body=media,
                            fields='id').execute()

    file_id = file_id.get('id')

    share(service, file_id)  # Share uploaded file

    return file_id


def get_files(file_id):
    """Returns a title of document in Google drive
    Sample:

    file_id = service.files().get(fileId='1G1yqEJdqH7rBgY7TN5fJnihQV-Vl7owNI1Bo8UiekOY').execute()
    """
    credentials = get_credentials()
    http = credentials.authorize(httplib2.Http())
    service = discovery.build('drive', 'v3', http=http)
    file_name = service.files().get(fileId=file_id).execute()

    return file_name['name']


if __name__ == '__main__':
    google_mime = 'application/vnd.google-apps.spreadsheet'
    mime_type = 'application/vnd.ms-excel'
    reader_email = 'arbinbulai@gmail.com'
    filename = os.path.basename(r'D:\Programs\Chorus\trunk\src\input\Phase5_CHORUS(DOE DOD NSF USDA NIST USGS Smithsonian Gates)_PDF.xls')
    file_input = r'D:\Programs\Chorus\trunk\src\input\Phase5_CHORUS(DOE DOD NSF USDA NIST USGS Smithsonian Gates)_PDF.xls'
    title = os.path.splitext(filename)[0]
    fid = upload(title, file_input, google_mime, mime_type, reader_email)
    base_url = 'https://docs.google.com/spreadsheets/d/%s' % fid
    print(base_url)

    #get_files()