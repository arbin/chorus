"""
__author__ = 'abulaybulay'
__about__ = 'worksheet read and write'
"""

import os
import os.path
from xlrd import open_workbook
from xlutils.copy import copy


def write_worksheet(spread_file, row_number, column_name, cell_value):
    """Write or edit existing excel documents """
    update_columns = {'status': 17, 'download_date': 18, 'success': 19, 'error_code': 20, 'remarks': 21} # Column number specific for update
    try:
        rb = open_workbook(spread_file)
        wb = copy(rb)
        s = wb.get_sheet(0)
        s.write(row_number, update_columns[column_name], cell_value)
        wb.save(spread_file)
    except Exception as e:
        print(e)


def read_worksheet(spread_file):
    """Read worksheet and return list of values """
    list_values = []
    none_cell_value = ['PII', '']
    try:
        if os.path.isfile(spread_file):
            print(spread_file)
            rb = open_workbook(spread_file)
            rb.sheet_names()
            sheet = rb.sheet_by_index(0)
            #headers = sheet.row_values(1)
            #print(headers)
            for rownum in range(sheet.nrows):
                rows = sheet.row_values(rownum)
                pii = str(rows[0]).strip("\n")
                #print(rownum, pii)
                if pii not in none_cell_value:
                    list_values.append(pii)
                """
                except_rownum = [0, 1]
                if rownum not in except_rownum:
                    column_name = 'download_date'
                    write_worksheet(spread_file, rownum, column_name, pii)
                """
            return list_values
    except Exception as e:
        print(e)


if __name__ == "__main__":
    spread_file = r'D:\Programs\AM4GreenDownloader\Phase5.xls'
    read_worksheet(spread_file)
