import datetime
import logging
import os
import requests
import sys
import time
import zipfile

from decorator import decorator
from requests.auth import HTTPDigestAuth

#import ziptools


class RetryCall(object):
    def __init__(self, tries, pause):
        self.tries = tries
        self.pause = pause

    def __call__(self, f, *args, **kwargs):
        i = 0
        while True:
            i += 1
            try:
                return f(*args, **kwargs)
            except:
                if i>=self.tries: # >= as tries might be negative (or 0)
                    logging.debug('Call to %s with %r failed' % (f.func_name,
                                    (args, kwargs)))
                    raise

            time.sleep(self.pause)
            logging.debug('Retrying call to %s...' % f.func_name)


def retrycall(tries, pause):
    return lambda f: decorator(RetryCall(tries, pause), f)


@retrycall(6, 5.0)
def retr(s, u, callback):
    headers = { 'User-Agent': 'Mozilla/5.0 (Windows NT 6.0; WOW64; rv:24.0) Gecko/20100101 Firefox/24.0' }

    r = s.get(u)#, headers=headers)
    try:
        print (r.status_code, r.reason), u
        callback(r.content)
    finally:
        r.close()


def mod_time(s):
    d = datetime.datetime.strptime(s[:19], '%Y-%m-%dT%H:%M:%S')

    #return (d.year, d.month, d.day, d.hour, d.minute, d.second)
    return (d.day, d.month, d.year, d.hour, d.minute, d.second)


def get_latest(names):
    ret = {}
    for k, v in names.iteritems():
        v.sort() # latest will be last entry
        ret[k] = v[-1]

    return ret


def get_names(j):
    names = {}
    for d in j['bam:hasAssetMetadata']:
        u = d['@id']
        n = d['bam:filename']
        if not n: continue # may be None
        names.setdefault(n, []).append((d['prov:generatedAtTime'],
                                        d['prism:byteCount'], u))

    return get_latest(names)


if __name__ == '__main__':
    logging.getLogger().setLevel(logging.DEBUG)
    top = r'D:\Programs\24hr_downloader'
    pii = 'S2214782915300099'

    s = requests.Session()
    s.auth = HTTPDigestAuth('spi-24pub', 'Spi2vtwprod')

    try:
        u = 'https://vtw.elsevier.com/assetMetadata/pii/%s' % (pii,)
        r = s.get(u)
        try:
            print (r.status_code, r.reason), u
            j = r.json()
        finally:
            r.close()

        names = get_names(j)

        begin = time.time()

        zp = os.path.join(top, '%s.zip' % (pii,))
        zf = zipfile.ZipFile(zp, 'w', zipfile.ZIP_DEFLATED, True)
        try:
            for n, (timestamp, size, url) in names.iteritems():
                nfo = zipfile.ZipInfo(n, mod_time(timestamp))
                cb = lambda data: zf.writestr(nfo, data)
                retr(s, url, cb)
                if zf.getinfo(nfo.filename).file_size!=size:
                    print (zf.getinfo(nfo.filename).file_size, size, nfo.filename)
        finally:
            zf.close()

        end = time.time()
        print end-begin

    finally:
        s.close()
