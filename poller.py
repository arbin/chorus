__author__ = 'abulaybulay'
__about__ = """poller.py
            - poll input and run executable simultaneously
            """

from configobj import ConfigObj
import logging
import os
import subprocess
import time


logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logging.getLogger('requests').setLevel(logging.CRITICAL)
logger = logging.getLogger(__name__)

config = ConfigObj('config.ini')


def poller():
    while 1:
        for filename in os.listdir(config['paths']['input']):
            if filename.endswith('.xls'):
                logger.info('Processing %s' % filename)
                #subprocess.call(os.path.join(os.getcwd(), 'chorus.exe'))
                subprocess.call("chorus.py", shell=True)
            print('Waiting for new input...')
            time.sleep(float(config['interval']['waiting']))


if __name__ == '__main__':
    poller()
