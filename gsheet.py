"""
__name__ = 'gsheet.py'
__author__ = 'abulaybulay'
__about__ = 'Google sheets cell update'
"""

import gspread
import json
import os

import retry

from oauth2client.service_account import ServiceAccountCredentials



scope = ['https://spreadsheets.google.com/feeds',
        'https://www.googleapis.com/auth/drive']
credentials = ServiceAccountCredentials.from_json_keyfile_name(os.getcwd() + '/creds/chorus-gspread.json', scope)


@retry.retry(gspread, tries=2, delay=3, backoff=2)
def authorize(credentials):
    g_client = gspread.authorize(credentials)
    return g_client


def make_public(file_id, shared_email):
    """Share the spreadsheet to a given google user.
    role(s): writer, reader, owner
    """
    perms_url = 'https://www.googleapis.com/drive/v2/files/{0}/permissions'.format(file_id)
    headers = {'Content-Type': 'application/json'}
    g_client = authorize(credentials)
    g_client.session.connections = {}
    data = {'role': 'writer', 'type': 'user', 'value': shared_email}
    g_client.session.request('POST',
        perms_url,
        headers=headers,
        data=json.dumps(data)
    )


def get_acell(title, cell_id):
    g_client = authorize(credentials)
    spread = g_client.open(title).sheet1
    print str(spread.acell(cell_id).value)
    return spread.acell(cell_id).value


@retry.retry(gspread.SpreadsheetNotFound, tries=2, delay=3, backoff=2)
def update_cell_error(title, pii, unformatted_pii, pii_cell, status_cell,
                        date_cell, successful_cell, error_code_cell,
                        error_code_value, remarks_cell, remarks_value,
                        date_cell_value):
    g_client = authorize(credentials)
    spread = g_client.open(title).sheet1
    none_cell_value = ['PII', '']
    if spread.acell(pii_cell).value not in none_cell_value:
        print('Updating error status on Google sheets for %s' % pii)
        print(pii_cell, error_code_cell, remarks_cell)
        print('PII cell value is %s' % spread.acell(pii_cell).value)
        print('Unformatted pii is %s' % unformatted_pii)
        if spread.acell(pii_cell).value == unformatted_pii:
            try:
                spread.update_acell(status_cell, 'Downloading Error')
                spread.update_acell(date_cell, date_cell_value)
                spread.update_acell(successful_cell, 'No')
                spread.update_acell(error_code_cell, error_code_value)
                spread.update_acell(remarks_cell, remarks_value)
                print('Done updating pii: %s' % unformatted_pii)
                return True
            except Exception as e:
                print e


@retry.retry(gspread.SpreadsheetNotFound, tries=2, delay=3, backoff=2)
def update_cell(title, pii, unformatted_pii, pii_cell, status_cell, date_cell,
                successful_cell, error_code_cell,
                remarks_cell, date_cell_value):
    """(title, pii, pii_cell, date_cell,
                successful_cell, error_code_cell, remarks_cell,
                str(datetime.date.today()))
    Update cell with given value and cell id
    Example:
        spread = g_client.open(title).sheet1
        spread.update_acell('D1', "DDD1234567890")
        cell_list = spread.range('A1:D7')
    """
    g_client = authorize(credentials)
    spread = g_client.open(title).sheet1
    print('Updating pii status on Google sheets for %s' % pii)
    print('Unformatted pii: %s' % unformatted_pii)
    print(pii_cell, date_cell, successful_cell, error_code_cell, remarks_cell,
            date_cell_value)
    print('PII cell value is %s' % spread.acell(pii_cell).value)
    if spread.acell(pii_cell).value == unformatted_pii:
        try:
            spread.update_acell(status_cell, 'Downloaded')
            spread.update_acell(date_cell, date_cell_value)
            spread.update_acell(successful_cell, 'Yes')
            spread.update_acell(error_code_cell, '')
            spread.update_acell(remarks_cell, '')
            print('Done updating pii: %s' % unformatted_pii)
            return True
        except Exception as e:
            print e


if __name__ == '__main__':
    update_cell('Phase5_CHORUS(DOE DOD NSF USDA NIST USGS Smithsonian Gates)_PDF',
    'S0001691816300968', 'S0001-6918(16)30096-8', 'A10',
    'S10', 'T10', 'U10', 'V10', '2016-12-21')
    #get_acell('Phase5_CHORUS(DOE DOD NSF USDA NIST USGS Smithsonian Gates)_PDF', 'A3')