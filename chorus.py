__author__ = 'abulaybulay'
__about__ = """Chorus App.
            - Multi-threading downloads of assets with a given PII.
            - Uploads to Tracking sheet to Google Drive.
            - Update Tracking sheet using Google Sheets
            """

import ctypes
import datetime
import logging
import os
import json
import requests
import shutil
import time

from configobj import ConfigObj
from datetime import timedelta
from functools import partial
from multiprocessing.dummy import Pool as ThreadPool
from requests.auth import HTTPDigestAuth
from cryptor import decrypt

import lspread
import gdrive
import gsheet

from functools import wraps


ctypes.windll.kernel32.SetConsoleTitleA("Chorus")

logging.basicConfig(filename='logfile.log', level=logging.DEBUG,
            format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logging.getLogger('requests').setLevel(logging.CRITICAL)
logger = logging.getLogger(__name__)

date = datetime.date.today() - timedelta(1)
today = datetime.date.today()
today = today.strftime('%m%d%Y')

config = ConfigObj('config.ini')
username = decrypt(config['vtw_credentials']['username'], 'DESCRYPT')
password = decrypt(config['vtw_credentials']['password'], 'DESCRYPT')
s = requests.Session()
s.auth = HTTPDigestAuth(username, password)
resume_header = {'Range': 'bytes=%d-' % 0}
error_list = []


def retry(ExceptionToCheck, tries=2, delay=3, backoff=2, logger=None):
    def deco_retry(f):

        @wraps(f)
        def f_retry(*args, **kwargs):
            mtries, mdelay = tries, delay
            while mtries > 1:
                try:
                    return f(*args, **kwargs)
                except ExceptionToCheck as e:
                    msg = "%s, Retrying in %d seconds..." % (str(e), mdelay)
                    if logger:
                        logger.warning(msg)
                    else:
                        print msg
                    time.sleep(mdelay)
                    mtries -= 1
                    mdelay *= backoff
            return f(*args, **kwargs)

        return f_retry  # true decorator

    return deco_retry


def get_latest_version(version):
    """Get the latest version in assets
    """
    version.sort()
    latest = version[-1]
    return latest


def get_final_list(list_asset):
    """Exclude items from the list of urls. """
    exclude_items = config['links']['remove']
    for i in exclude_items:
        asset = [item for item in list_asset if i not in item]
    for i in exclude_items:
        asset = [item for item in asset if i not in item]
    return asset


def read_json(json_file):
    """Read metadata json file and get the asset list under specified stage and
    version mentioned below. Test with a json file
    data = codecs.open(json_file).read()
    asset_metadata = json.loads(data)
    """
    asset_metadata = json.loads(json_file)
    urls = asset_metadata["bam:hasGeneration"]
    stage = ['accepted_manuscript', 'accepted_original']
    version = []
    download_stage = config['action']['download_stage']

    if download_stage == 'S0':
        for url in urls:
            if url["bam:stage"] in stage and "S0." in url["bam:contributorVersionIdentifier"]:
                version.append(str(url["bam:contributorVersionIdentifier"]))
            if url["bam:stage"] in stage and url["bam:contributorVersionIdentifier"] != "S5.1":
                version.append(str(url["bam:contributorVersionIdentifier"]))

    elif download_stage == 'S5':
        for url in urls:
            if url["bam:stage"] in stage and "S5." in url["bam:contributorVersionIdentifier"]:
                version.append(str(url["bam:contributorVersionIdentifier"]))

    if version:
        latest = get_latest_version(version)
        for url in urls:
            if url["bam:stage"] == 'accepted_manuscript' and url["bam:contributorVersionIdentifier"] == latest:
                has_asset = url["bam:hasAsset"]
            elif url["bam:stage"] == 'accepted_original' and url["bam:contributorVersionIdentifier"] == latest:
                has_asset = url["bam:hasAsset"]
        return has_asset
    else:
        return None


@retry(requests.HTTPError, tries=2, delay=3, backoff=2)
def get_asset_details(pii, asset):
    """Get the asset metadata details. """
    asset_metadata_url = config['paths']['asset_metadata_url'].replace('<pii>', pii)
    try:
        logger.info('Calling Asset Metadata API URL %s' % asset_metadata_url)
        response = s.get(asset_metadata_url, auth=HTTPDigestAuth(username, password),
                    headers=resume_header, stream=True,
                    allow_redirects=True, verify=False)
        if response.status_code == 200:
            asset_metadata = json.loads(response.text)
            for_download = []
            for item in asset:
                for d in asset_metadata['bam:hasAssetMetadata']:
                    if item == d['@id']:
                        j = {"@id": item, "filename": d['bam:filename'], "byte": d['prism:byteCount']}
                        for_download.append(json.dumps(j))
            return for_download
        else:
            with open(os.path.join(config['paths']['failed_log'],
                    '%s.csv') % today, 'a') as f:
                    f.write('%s,%s,%s,%s' % (pii, str(response.status_code), str(response.reason),
                    asset_metadata_url + '\n'))
    finally:
        print(response.status_code, response.reason)
        response.close()


@retry(requests.HTTPError, tries=2, delay=3, backoff=2)
def get_asset_metadata(title, pii, unformatted_pii, pii_cell, status_cell, date_cell,
                successful_cell, error_code_cell, remarks_cell):
    """Get the json metadata and return the list of api asset urls to be
    called for each asset.
    """
    metadata_url = config['paths']['metadata_url'].replace('<pii>', pii)
    logger.info('Calling Metadata API URL %s' % metadata_url)
    try:
        response = s.get(metadata_url, auth=HTTPDigestAuth(username, password),
                    headers=resume_header, stream=True,
                    allow_redirects=True, verify=False)

        if response.status_code == 200:
            logger.info('Creating folder for %s' % pii)
            if not os.path.exists(os.path.join(config['paths']['local'], pii)):
                os.mkdir(os.path.join(config['paths']['local'], pii))
            asset = read_json(response.text)
            if asset != None:
                return asset
            else:
                with open(os.path.join(config['paths']['failed_log'], '%s.csv') % today, 'a') as f:
                    f.write('%s,%s,%s,%s' % (pii, 'Error 2', 'No S0 in VTW', metadata_url + '\n'))
                remarks_value = 'S0/R1V0/R1V1 or any needed materials are not available in vtw.elsevier.com/metadata/pii/%s' % pii
                gsheet.update_cell_error(title, pii, unformatted_pii,
                    pii_cell, status_cell, date_cell, successful_cell, error_code_cell, 'Error 3',
                    remarks_cell, remarks_value, str(datetime.date.today()))
                return None
        else:
            logger.info('No asset found for %s' % pii)
            with open(os.path.join(config['paths']['failed_log'],
                '%s.csv') % today, 'a') as f:
                f.write('%s,%s,%s,%s' % (pii, str(response.status_code),
                str(response.reason), metadata_url + '\n'))
            remarks_value = str(response.status_code) + ' - ' + str(response.reason) + ' - ' + ('vtw.elsevier.com/metadata/pii/%s' % pii)
            gsheet.update_cell_error(title, pii, unformatted_pii, pii_cell, status_cell, date_cell,
                successful_cell, error_code_cell, 'Error 3', remarks_cell,
                remarks_value, str(datetime.date.today()))
            return None
    finally:
        print(response.status_code, response.reason)
        response.close()


@retry(requests.HTTPError, tries=2, delay=3, backoff=2)
def get_links(url):
    """Download assets by calling each url
    """
    url = json.loads(url)
    total_size = url["byte"]
    filename = url['filename']
    url_path = url['@id']
    pii = url_path[34:51] #VTW live
    #pii = url_path[38:55] #VTW Acceptance
    try:
        logger.info('Calling API URL %s', url['@id'])
        url = url['@id'].encode("cp437", "ignore")

        response = s.get(url, headers=resume_header, stream=True,
                        verify=False, allow_redirects=True)

        if response.status_code == 206:
            print (('Now downloading %s' % filename.encode("cp437", "ignore")))
            chunk_size = 32768
            bytes_read = 0
            if filename != 'null':
                #if not os.path.isfile(os.path.join(config['paths']['local'] + pii + '/', '%s' + u'') % filename):
                if not os.path.isfile(os.path.join(config['paths']['local'],
                        "%s/%su''" % (pii, filename))):
                    with open(os.path.join(config['paths']['local'] + pii
                            + '/', '%s' + u'') % filename, 'wb') as handle:

                        for chunk in response.iter_content(chunk_size):
                            handle.write(chunk)
                            handle.flush()
                            os.fsync(handle.fileno())
                            bytes_read += len(chunk)
                            percent_value = 100 * float(bytes_read) / float(total_size)
                            print '\rProgress: ' + str(bytes_read) + ' (' + str(percent_value) + '%) of ' + str(total_size) + '(Total Size)',
                        handle.close()
                        with open(os.path.join(config['paths']['download_log'],
                            '%s.csv') % today, 'a') as f:
                            f.write('%s,%s,%s,%s' % (pii, str(response.status_code),
                            str(response.reason), url + '\n'))

                        if int(bytes_read) != int(total_size):
                            error = ('Downloaded file size of ')
                            + pii + ' is not the same with declared'
                            ' content headers file size. '
                            'Please check this. '
                            error_list.append({'url': url_path, 'filename': str(filename),
                                                'error': error})
                            print error
                            return 'Error1'
                            #remarks_value = 'Unable to open/unzip, or has no content'
                            #gsheet.update_cell_error(title, pii, unformatted_pii,
                            #    pii_cell, successful_cell, error_code_cell,
                            #    'Error 1', remarks_cell, remarks_value)
            return 'NoError'
        else:
            error_list.append({'url': url_path, 'filename': str(filename), 'error': str(response.status_code) + ' - ' + str(response.reason)})
            with open(os.path.join(config['paths']['failed_log'], '%s.csv') % today, 'a') as f:
                f.write('%s,%s,%s,%s' % (pii, 'Error 3',
                'InvalidObjectState', url_path + '\n'))
    except requests.exceptions.RequestException as msg:
        print(msg)
        error = filename + ' ' + str(datetime.datetime.strftime(datetime.datetime.now(),
            '%Y-%m-%dT%H:%M:%S')) + '\n' + str(msg) + '\n\n' + url['@id']
        error_list.append({'url': url_path, 'filename': filename, 'error': error})
        return 'Error3'
    finally:
        response.close()


def download(title, pii, unformatted_pii, pii_cell, status_cell, date_cell,
                successful_cell, error_code_cell, remarks_cell):
    """Download assets passing the pii from a given list.  A json response
    with a list of given api urls are to be extracted and used as parameter
    for a get method http call.
    """
    threads = config['action']['download_threads']
    pool = ThreadPool(int(threads))
    list_asset = get_asset_metadata(title, pii, unformatted_pii, pii_cell,
                status_cell, date_cell, successful_cell, error_code_cell,
                remarks_cell)
    if list_asset:
        asset = get_final_list(list_asset)
        if asset:
            urls = get_asset_details(pii, asset)
            if urls:
                logger.info('Trying to download %s', pii)
                start = time.clock()
                stat = pool.map(partial(get_links), urls)
                print(('Returns %s ' % str(stat)))
                if 'Error1' in stat:
                    remarks_value = 'Unable to open/unzip, or has no content'
                    gsheet.update_cell_error(title, pii, unformatted_pii,
                        pii_cell, status_cell, date_cell, successful_cell, error_code_cell,
                        'Error 1', remarks_cell, remarks_value, str(datetime.date.today()))
                if 'Error3' in stat:
                    remarks_value = 'S0/R1V0/R1V1 or any needed materials are not available in vtw.elsevier.com/metadata/pii/%s' % pii
                    gsheet.update_cell_error(title, pii, unformatted_pii, pii_cell, status_cell, date_cell,
                    successful_cell, error_code_cell, 'Error 3', remarks_cell,
                    remarks_value, str(datetime.date.today()))
                if 'NoError' in stat:
                    print('\nCalling Gsheet to update pii status on Google sheets for %s' % pii)
                    gsheet.update_cell(title, pii, unformatted_pii, pii_cell, status_cell, date_cell,
                        successful_cell, error_code_cell, remarks_cell,
                        str(datetime.date.today()))
                    return time.clock() - start


def start(filename):
    google_mime = 'application/vnd.google-apps.spreadsheet'
    mime_type = 'application/vnd.ms-excel'
    reader_email = config['google']['reader_email']
    title = os.path.basename(config['paths']['input'] + filename)
    title = os.path.splitext(filename)[0]
    input_file = config['paths']['input'] + filename
    #Upload file to google drive and get the File Id
    if not os.path.isfile(config['paths']['ongoing'] + filename):
        fid = gdrive.upload(title, input_file, google_mime, mime_type, reader_email)
        base_url = 'https://docs.google.com/spreadsheets/d/%s' % fid
        print(base_url)
        f = open(config['paths']['fid'] + str(fid) + '.fid', 'w')
        f.write(filename)
        f.close()

    shutil.move(config['paths']['input'] + filename, config['paths']['ongoing'] + filename)
    #Download items from sheet 1
    for_download = lspread.read_worksheet(config['paths']['ongoing'] + filename)
    base_row = 3
    pii_column = 'A'
    status_column = 'R'
    date_column = 'S'
    successful_column = 'T'
    error_code_column = 'U'
    remarks_column = 'V'
    for pii in for_download:
        unformatted_pii = pii
        pii = pii.replace('-', '')
        pii = pii.replace('(', '')
        pii = pii.replace(')', '')
        print(pii)
        pii_cell = pii_column + str(base_row)
        status_cell = status_column + str(base_row)
        date_cell = date_column + str(base_row)
        successful_cell = successful_column + str(base_row)
        error_code_cell = error_code_column + str(base_row)
        remarks_cell = remarks_column + str(base_row)
        download(title, pii, unformatted_pii, pii_cell, status_cell, date_cell,
            successful_cell, error_code_cell, remarks_cell)
        base_row += 1
    shutil.move(config['paths']['ongoing'] + filename, config['paths']['done'] + filename)


if __name__ == '__main__':
    google_mime = 'application/vnd.google-apps.spreadsheet'
    mime_type = 'application/vnd.ms-excel'
    reader_email = config['google']['reader_email']
    for filename in os.listdir(config['paths']['input']):
        title = os.path.basename(config['paths']['input'] + filename)
        title = os.path.splitext(filename)[0]
        input_file = config['paths']['input'] + filename
        if not os.path.isfile(config['paths']['ongoing'] + filename):
            fid = gdrive.upload(title, input_file, google_mime, mime_type, reader_email)
            base_url = 'https://docs.google.com/spreadsheets/d/%s' % fid
            print(base_url)
            f = open(config['paths']['fid'] + str(fid) + '.fid', 'w')
            f.write(filename)
            f.close()
        shutil.move(config['paths']['input'] + filename, config['paths']['ongoing'] + filename)
        for_download = lspread.read_worksheet(config['paths']['ongoing'] + filename)
        base_row = 3
        pii_column = 'A'
        status_column = 'R'
        date_column = 'S'
        successful_column = 'T'
        error_code_column = 'U'
        remarks_column = 'V'
        for pii in for_download:
            unformatted_pii = pii
            pii = pii.replace('-', '')
            pii = pii.replace('(', '')
            pii = pii.replace(')', '')
            print(pii)
            pii_cell = pii_column + str(base_row)
            status_cell = status_column + str(base_row)
            date_cell = date_column + str(base_row)
            successful_cell = successful_column + str(base_row)
            error_code_cell = error_code_column + str(base_row)
            remarks_cell = remarks_column + str(base_row)
            download(title, pii, unformatted_pii, pii_cell, status_cell, date_cell,
                successful_cell, error_code_cell, remarks_cell)
            base_row += 1
        shutil.move(config['paths']['ongoing'] + filename, config['paths']['done'] + filename)

