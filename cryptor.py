import pyDes, base64


def decrypt(data, key):
    decrypter = pyDes.des(key, mode=pyDes.CBC, IV="\0\1\2\3\4\5\6\7")
    decrypted_string = decrypter.decrypt(base64.b64decode(data), padmode=pyDes.PAD_PKCS5)
    return decrypted_string


if __name__ == '__main__':
    data = 'PMWy/PrwRf/d+/v0k1YTYA=='
    key = 'DESCRYPT'
    print decrypt(data, key)